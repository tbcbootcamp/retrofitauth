package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.login.HttpRequest.LOGIN
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        signInButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {
                logIn()
            } else {
                Toast.makeText(applicationContext, "Fill all fields!", Toast.LENGTH_SHORT).show()
            }
        }
        signUpTextView.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    private fun logIn() {
        val params = mutableMapOf<String, String>()
        params["email"] = emailEditText.text.toString()
        params["password"] = passwordEditText.text.toString()
        HttpRequest.postRequest(LOGIN, params, object : CustomCallback {

            override fun onSuccess(response: String, message: String) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
                    .show()
                messageTextView.text = message
            }

            override fun onError(response: String, message: String) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
                    .show()
                messageTextView.text = message
            }

            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "No Internet Connection", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}