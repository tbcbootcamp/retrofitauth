package com.example.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.login.HttpRequest.REGISTRATION
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        signUpButton.setOnClickListener {
            if (emailField.text.isNotEmpty() && passwordField.text.isNotEmpty() && confirmPasswordField.text.isNotEmpty()) {
                passwordMatch()
            } else {
                Toast.makeText(applicationContext, "Fill all fields!", Toast.LENGTH_SHORT).show()
            }
        }
        signInTextView.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    private fun signUp() {
        val params = mutableMapOf<String, String>()
        params["email"] = emailField.text.toString()
        params["password"] = passwordField.text.toString()
        HttpRequest.postRequest(REGISTRATION, params, object : CustomCallback {
            override fun onSuccess(response: String, message: String) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
                    .show()
                messageTextView2.text = message
            }

            override fun onError(response: String, message: String) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
                    .show()
                messageTextView2.text = message
            }

            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "No Internet Connection", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun passwordMatch() {
        if (passwordField.text.toString() != confirmPasswordField.text.toString()) {
            Toast.makeText(
                applicationContext, "Passwords don't match", Toast.LENGTH_SHORT
            ).show()
        } else {
            signUp()
        }
    }
}